
void LCD_ini(void)
{
  HAL_Delay(40);
  HAL_GPIO_WritePin(lcd_rs_GPIO_Port,lcd_rs_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
  LCD_WriteData(3);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
  
  HAL_Delay(5);
  HAL_GPIO_WritePin(lcd_rs_GPIO_Port,lcd_rs_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
  LCD_WriteData(3);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
  
  HAL_Delay(2);
  HAL_GPIO_WritePin(lcd_rs_GPIO_Port,lcd_rs_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
  LCD_WriteData(3);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
  
  HAL_Delay(2);
  HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
  HAL_Delay(2);
  LCD_Command(0x2c);//4bit,2 lines, 5*8 font
  HAL_Delay(2);
  LCD_Command(0x2c);//once again
  HAL_Delay(2);
  LCD_Command(0x2c);//once again
  HAL_Delay(2);
  LCD_Command(0x0C);//enable display, enable cursors
  HAL_Delay(2);
  LCD_Command(0x01);//disable cursor
  HAL_Delay(2);
  LCD_Command(0x06);//write to left.
  HAL_Delay(2);
  LCD_Command(0x02);//move cursor to zero position
  HAL_Delay(2);
}
  void delay(void)
{
  uint16_t i;
  for(i=0;i<1000;i++)
  {
    
  }
}
void LCD_WriteData(uint8_t dt)
{
  if(((dt >> 3)&0x01)==1) {HAL_GPIO_WritePin(lcd_d7_GPIO_Port,lcd_d7_Pin,GPIO_PIN_SET);} else {HAL_GPIO_WritePin(lcd_d7_GPIO_Port,lcd_d7_Pin,GPIO_PIN_RESET);}
  if(((dt >> 2)&0x01)==1) {HAL_GPIO_WritePin(lcd_d6_GPIO_Port,lcd_d6_Pin,GPIO_PIN_SET);} else {HAL_GPIO_WritePin(lcd_d6_GPIO_Port,lcd_d6_Pin,GPIO_PIN_RESET);}
  if(((dt >> 1)&0x01)==1) {HAL_GPIO_WritePin(lcd_d5_GPIO_Port,lcd_d5_Pin,GPIO_PIN_SET);} else {HAL_GPIO_WritePin(lcd_d5_GPIO_Port,lcd_d5_Pin,GPIO_PIN_RESET);}
  if((dt&0x01)==1) {HAL_GPIO_WritePin(lcd_d4_GPIO_Port,lcd_d4_Pin,GPIO_PIN_SET);} else {HAL_GPIO_WritePin(lcd_d4_GPIO_Port,lcd_d4_Pin,GPIO_PIN_RESET);}
}

void LCD_Command(uint8_t dt)
{
   HAL_GPIO_WritePin(lcd_rs_GPIO_Port,lcd_rs_Pin,GPIO_PIN_RESET);
   LCD_WriteData(dt>>4);
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
   delay();
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
   LCD_WriteData(dt);
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
   delay();
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
}
void LCD_Data(uint8_t dt)
{
   HAL_GPIO_WritePin(lcd_rs_GPIO_Port,lcd_rs_Pin,GPIO_PIN_SET);
   LCD_WriteData(dt>>4);
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
   delay();
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
   LCD_WriteData(dt);
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_SET);
   delay();
   HAL_GPIO_WritePin(lcd_e_GPIO_Port,lcd_e_Pin,GPIO_PIN_RESET);
}
void LCD_Clear(void)
{
        LCD_Command(0x01);
        HAL_Delay(1);
}
void LCD_SendChar(char ch)
{
        LCD_Data((uint8_t) ch);
        HAL_Delay(1);
}
void LCD_String(char* st)
{
  uint8_t i=0;
  while(st[i]!=0)
  {
    LCD_Data(st[i]);
    HAL_Delay(1);
    i++;
  }
}

void LCD_SetPos(uint8_t x, uint8_t y)
{
  switch(y)
  {
      case 0:
          LCD_Command(x|0x80);
          HAL_Delay(1);
          break;
      case 1:
          LCD_Command((0x40+x)|0x80);
          HAL_Delay(1);
          break;
      case 2:
          LCD_Command((0x14+x)|0x80);
          HAL_Delay(1);
          break;
      case 3:
          LCD_Command((0x54+x)|0x80);
          HAL_Delay(1);
          break;
   }
}