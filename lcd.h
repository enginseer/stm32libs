

void LCD_ini(void);
void LCD_WriteData(uint8_t dt);
void LCD_Command(uint8_t dt);
void LCD_Data(uint8_t dt);
void LCD_Clear(void);
void LCD_SendChar(char ch);
void delay(void);
void LCD_String(char* st);
void LCD_SetPos(uint8_t, uint8_t);
